var keywordExtractor = require('keyword-extractor');
var _ = require('underscore');
var async = require('async');
var req = require('rekuire');

var db = req('database');

module.exports = {

  getFrontier: function(callback) {
    /** Get URLS from FRONTIER **/
    var conn = db.getConnection();
    var query = 'SELECT * FROM FRONTIER';
    db.startConnection(conn);
    conn.query(query, function(err, rows, fields){
      if(err) {
        console.log('Error while performing query..');
        console.log(err);
        return callback(err);
      }
      return callback(null, rows);
    });
  },

  getCrawledUrls: function(callback) {
    console.log('Getting crawled urls..');
    var conn = db.getConnection();
    var query = 'SELECT URL FROM CRAWLED_URLS';
    conn.query(query, function(err, rows, fields){
      if(err) {
        console.log('Error while performing query..');
        console.log(err);
        return callback(err);
      } 
      return callback(null, rows);
    });
  },

  insertMultipleData: function(data, callback) {
    var conn = db.getConnection();
    if(data.length == 0) {
      console.log('Nothing to save');
      return callback(null, conn);
    }
    
    var i, j, temparray, parsedArray=[], chunk = 50;
    for (i=0,j=data.length; i<j; i+=chunk) {
        temparray = data.slice(i, i+chunk);
        parsedArray.push(temparray);
    }

    console.log('Inserting multiple data...');
    async.times(parsedArray.length, function(n, next){
      var toInsert = parsedArray[n];
      var query = 'INSERT INTO CRAWLED_URLS (FRONTIER_ID, URL, TITLE, KEYWORD, DEPTH, BODY) VALUES ?';
      var values = [];

      for(var i=0; i<toInsert.length; i++) {
        var keyword = keywordExtractor.extract(data[i].body);
        var v = [toInsert[i].frontierId, toInsert[i].url, toInsert[i].title, _.uniq(keyword).toString(), toInsert[i].depth, toInsert[i].body];
        values.push(v);
      }

      conn.query(query, [values], function(err, saved){
        if(err) {
          console.log(err);
          return callback(err);
        }
        next(err, saved);
      });

    }, function(error){
      return callback(null, conn);
    });
  },

  insertTimeStamps: function(callback, data) {
    var conn = db.getConnection();

    console.log('Inserting timestamps..');
    var query = 'INSERT INTO LOGS (START_TIME, END_TIME) VALUES (' + data.startTime + ',' + data.endTime + ')';
    conn.query(query, function(err){
      if(err) {
        console.log(err);
        return callback(err);
      }
      return callback(null, conn);
    });
  }

};