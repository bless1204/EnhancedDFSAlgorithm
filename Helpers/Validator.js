var validUrl = require('valid-url');

module.exports = {

  isValidUrl: function(url) {
    return validUrl.isUri(url);
  }

};